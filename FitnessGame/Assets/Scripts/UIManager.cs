using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {
    public static UIManager instance;
    [Header("Main UI Elements")]
    [SerializeField] private TextMeshProUGUI topText;
    [SerializeField] private TextMeshProUGUI timerText;
    [SerializeField] private TextMeshProUGUI centerText;
    [SerializeField] private TextMeshProUGUI bottomText;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private GameObject mainMenuButton;

    [Header("Background Color")]
    public float hue = 1f;
    [SerializeField] private float speed = 0.1f;

    void Awake() {
        if (instance == null) {
            instance = this;
            DontDestroyOnLoad(gameObject);
            ClearUI();
        } else {
            Destroy(gameObject);
        }
    }

    private void Update() {
        hue = (Time.time * speed) % 1;
    }

    public void ClearUI() {
        topText.text = "";
        timerText.text = "";
        centerText.text = "";
        bottomText.text = "";
        scoreText.text = "";
        mainMenuButton.SetActive(false);
    }

    public void ChangeTopText(string text) {
        topText.text = text;
    }

    public void ChangeTimerText(string text) {
        timerText.text = text;
    }

    public void ChangeCenterText(string text) {
        centerText.text = text;
    }

    public void ChangeBottomText(string text) {
        bottomText.text = text;
    }

    public void ChangeScoreText(string text) {
        scoreText.text = text;
    }

    public void ShowMainMenuButton() {
        mainMenuButton.SetActive(true);
    }

    public void HideMainMenuButton() {
        mainMenuButton.SetActive(false);
    }

    public void LoadMainMenu() {
        SceneManager.LoadScene("MainMenu");
    }
}
