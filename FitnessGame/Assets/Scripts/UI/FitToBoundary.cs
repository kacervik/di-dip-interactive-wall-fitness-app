using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FitToBoundary : MonoBehaviour {

    private void Update() {
        if (GameBoundary.instance.boundariesSet) {
            ResizeRectangle(GameBoundary.instance.minBound, GameBoundary.instance.maxBound);
        }
    }

    public void ResizeRectangle(Vector3 corner1, Vector3 corner2) {
        // Calculate the center position of the rectangle
        Vector3 center = (corner1 + corner2) / 2f;

        // Calculate the size of the rectangle
        float width = Mathf.Abs(corner1.x - corner2.x);
        float height = Mathf.Abs(corner1.y - corner2.y);

        // Set the position of the rectangle
        transform.position = center;

        // Set the size of the rectangle
        transform.localScale = new Vector3(width, height, 1f);
    }
}
