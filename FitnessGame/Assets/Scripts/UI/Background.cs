using UnityEngine;

public class Background : MonoBehaviour {
    SpriteRenderer spriteRenderer;

    private void Awake() {
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

    private void Update() {
        ChangeColor();
    }

    private void ChangeColor() {
        // Change color of the sprite with time
        // Get the current color of the SpriteRenderer
        Color currentColor = spriteRenderer.color;

        // Convert the current color to HSV
        Color.RGBToHSV(currentColor, out float H, out float S, out float V);

        // Change the hue of the sprite with time

        // Set the new color of the SpriteRenderer
        spriteRenderer.color = Color.HSVToRGB(UIManager.instance.hue, S, V);
    }
}
