using UnityEngine;
using UnityEngine.SceneManagement;

public class GameBoundary : BaseInteractive {

    public static GameBoundary instance;
    public Vector3 minBound, maxBound;
    // public Vector2 hitPointMinBound, hitPointMaxBound;
    public bool boundariesSet = false;

    [SerializeField] private Camera cam;
    [SerializeField] private bool isSettingBoundaries = false;
    [SerializeField] private bool isSettingMinBoundary = false;
    [SerializeField] private bool isSettingMaxBoundary = false;

    private void Awake() {
        if (instance == null) { // Only one instance of GameBoudary
            instance = this;
            DontDestroyOnLoad(gameObject);
        } else {
            Destroy(gameObject);
        }
    }

    void Start() {
        Debug.Log("GameBoundary Start");
        StartSettingBoundaries();
    }

    public void StartSettingBoundaries() {
        isSettingBoundaries = true;
        isSettingMinBoundary = true;
        UIManager.instance.ChangeTimerText("");
        UIManager.instance.ChangeTopText("Touch most bottom left part of the wall.");
    }

    private void Update() {
        // if (isSettingBoundaries) {
        //     SetPlayableAreaBoundaries();
        // }
    }

    public override void OnPress(Vector2 hitPosition) {
        if (isSettingBoundaries) {
            if (isSettingMinBoundary) {
                minBound = hitPosition;
                isSettingMinBoundary = false;
                isSettingMaxBoundary = true;
                UIManager.instance.ChangeTopText("Touch most top right part of the wall.");
            } else if (isSettingMaxBoundary) {
                maxBound = hitPosition;
                isSettingMaxBoundary = false;
                UIManager.instance.ChangeTopText("Boundaries Set");
                UIManager.instance.ChangeCenterText("OK");
                UIManager.instance.ChangeBottomText("Touch to continue.");
                boundariesSet = true;
            } else {
                isSettingBoundaries = false;
                SceneManager.LoadScene("MainMenu");
            }
        }
    }

    private void SetPlayableAreaBoundaries() {
        if (isSettingMinBoundary) {
            if (Input.GetMouseButtonDown(0)) {
                if (cam == null) {
                    cam = Camera.main;
                }
                minBound = cam.ScreenToWorldPoint(Input.mousePosition);

                // Initi Wall Hitboxes
                RaycastHit2D hit = Physics2D.Raycast(minBound, Vector2.zero);
                if (hit.collider != null) {
                    // hitPointMinBound = hit.point;
                }

                isSettingMinBoundary = false;
                isSettingMaxBoundary = true;
                UIManager.instance.ChangeTopText("Touch most top right part of the wall.");
            }
        } else if (isSettingMaxBoundary) {
            if (Input.GetMouseButtonDown(0)) {
                if (cam == null) {
                    cam = Camera.main;
                }
                maxBound = cam.ScreenToWorldPoint(Input.mousePosition);
                // Initi Wall Hitboxes
                RaycastHit2D hit = Physics2D.Raycast(minBound, Vector2.zero);
                if (hit.collider != null) {
                    // hitPointMinBound = hit.point;
                }
                isSettingMaxBoundary = false;
            }
        }

        if (isSettingMinBoundary == false && isSettingMaxBoundary == false) {
            UIManager.instance.ChangeTopText("Boundaries Set");
            isSettingBoundaries = false;
            boundariesSet = true;
            SceneManager.LoadScene("MainMenu");
        }
    }


    public void Reset() {
        isSettingBoundaries = true;
        isSettingMinBoundary = true;
        isSettingMaxBoundary = false;
        boundariesSet = false;
        minBound = Vector3.zero;
        maxBound = Vector3.zero;
    }

    // On scene change, start setting boundaries
    private void OnSceneLoaded(Scene scene, LoadSceneMode mode) {
        if (scene.name == "BoundarySetting") {
            Debug.Log("scene loaded: BoundarySetting");
            Reset();
            StartSettingBoundaries();
        }
    }
    private void OnEnable() {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable() {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
    private void OnDrawGizmos() {
        Gizmos.color = Color.green;
        DrawRectangle(minBound, maxBound);
    }

    private void DrawRectangle(Vector3 bottomLeft, Vector3 topRight) {
        Vector3 topLeft = new Vector3(bottomLeft.x, topRight.y, 0);
        Vector3 bottomRight = new Vector3(topRight.x, bottomLeft.y, 0);

        Gizmos.DrawLine(bottomLeft, topLeft);
        Gizmos.DrawLine(topLeft, topRight);
        Gizmos.DrawLine(topRight, bottomRight);
        Gizmos.DrawLine(bottomRight, bottomLeft);
    }
}
