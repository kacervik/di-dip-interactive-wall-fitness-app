using UnityEngine;

public class ExerciseArmsMovement : ExerciseBase {
    [SerializeField] private float exerciseDuration = 30f;
    private string exerciseName = "Arms Movement";
    private string exerciseDescription = "Move your arms to the target!";
    [SerializeField] private Vector3 lastPosition, newPosition;
    [SerializeField] private float radius = 3f, offset = 0.25f;

    private void Awake() {
        lastPosition = GenerateRandomPositionInBounds();
        newPosition = GenerateRandomPositionInBounds();
    }

    private void Update() {
        if (isActive) {
            Countdown();
            if (currentPrefabInstance == null) {
                SpawnPrefabs(PrefabLibrary.instance.prefabArmMovement, GenerateRandomPositionForArmsMovementTarget());
            }
        }
    }

    // Generate random position in the bounds of the game, but in reachable distance
    private Vector3 GenerateRandomPositionForArmsMovementTarget() {
        while (true) {
            newPosition = new Vector3(Random.Range(GameBoundary.instance.minBound.x, GameBoundary.instance.maxBound.x),
                                    Random.Range(GameBoundary.instance.minBound.y, GameBoundary.instance.maxBound.y),
                                    0);

            float distance = Vector3.Distance(newPosition, lastPosition);

            if (radius + offset > distance && distance > radius - offset) {
                break;
            }
        }

        lastPosition = newPosition;
        return newPosition;
    }

    public override void StartExercise() {
        InitializeExercise(exerciseName, exerciseDuration, exerciseDescription);
    }

    void OnDrawGizmos() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(lastPosition, radius);
        Gizmos.DrawWireSphere(newPosition, radius);
    }
}
