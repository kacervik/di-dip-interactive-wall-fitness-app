using UnityEngine;

public class ExerciseLegRaises : ExerciseBase {
    [SerializeField] private float exerciseDuration = 30f;
    private string exerciseName = "Leg Raises";
    private string exerciseDescription = "Stand and lift legs to side!";

    private GameObject silhouette;

    private void Start() {
        silhouette = Instantiate(PrefabLibrary.instance.silhouette);
        silhouette.SetActive(false);
        silhouette.transform.position = new Vector3(0, -0.9f, 0);
        silhouette.transform.localScale = new Vector3(-0.4f, 0.4f, 0.4f);
    }

    private void Update() {
        if (isActive) {
            silhouette.SetActive(true);
            Countdown();
            if (countdown > 15) {
                UIManager.instance.ChangeCenterText("Left Leg");
            } else if (countdown > 0) {
                silhouette.transform.localScale = new Vector3(+0.4f, 0.4f, 0.4f);
                UIManager.instance.ChangeCenterText("Right Leg");
            } else {
                UIManager.instance.ChangeCenterText("");
                Destroy(silhouette);
            }
        } else {
        }
    }

    public override void StartExercise() {
        InitializeExercise(exerciseName, exerciseDuration, exerciseDescription);
    }
}
