using UnityEngine;

public class ExerciseCore : ExerciseBase {
    [Header("Exercise Settings")]
    [SerializeField] private float exerciseDuration = 30f;
    private string exerciseName = "Core Exercise";
    private string exerciseDescription = "Reach up and down!";

    [Header("Spawning")]
    [SerializeField] private float offsetVertical = 1.5f;
    [SerializeField] private float offsetHorizontal = 3f;
    [SerializeField] private bool topHalf = true;
    [SerializeField] private float lastPosition, newPosition;
    private void Awake() {
        lastPosition = GenerateRandomPositionInBounds().x;
        newPosition = GenerateRandomPositionInBounds().x;
    }

    private void Update() {
        if (isActive) {
            Countdown();
            if (currentPrefabInstance == null) {
                SpawnPrefabs(PrefabLibrary.instance.prefabBase, GenerateRandomPositionForCoreTarget());
            }
        }
    }

    public override void StartExercise() {
        InitializeExercise(exerciseName, exerciseDuration, exerciseDescription);
    }

    private Vector3 GenerateRandomPositionForCoreTarget() {
        Vector3 position = Vector3.zero;

        while (true) {
            newPosition = Random.Range(GameBoundary.instance.minBound.x, GameBoundary.instance.maxBound.x);

            float distance = Mathf.Abs(newPosition - lastPosition);

            if (distance < offsetHorizontal) {
                break;
            }
        }

        if (topHalf) {
            topHalf = !topHalf;
            position = new Vector3(newPosition,
                                    Random.Range(GameBoundary.instance.maxBound.y - offsetVertical, GameBoundary.instance.maxBound.y),
                                    0);
        } else {
            topHalf = !topHalf;
            position = new Vector3(newPosition,
                                    Random.Range(GameBoundary.instance.minBound.y, GameBoundary.instance.minBound.y + offsetVertical),
                                    0);
        }

        lastPosition = newPosition;
        return position;
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.cyan;
        DrawRectangle(new Vector3(GameBoundary.instance.minBound.x, GameBoundary.instance.maxBound.y - offsetVertical, 0),
                        new Vector3(GameBoundary.instance.maxBound.x, GameBoundary.instance.maxBound.y, 0));

        Gizmos.color = Color.cyan;
        DrawRectangle(new Vector3(GameBoundary.instance.minBound.x, GameBoundary.instance.minBound.y, 0),
                        new Vector3(GameBoundary.instance.maxBound.x, GameBoundary.instance.minBound.y + offsetVertical, 0));
    }

    private void DrawRectangle(Vector3 bottomLeft, Vector3 topRight) {
        Vector3 topLeft = new Vector3(bottomLeft.x, topRight.y, 0);
        Vector3 bottomRight = new Vector3(topRight.x, bottomLeft.y, 0);

        Gizmos.DrawLine(bottomLeft, topLeft);
        Gizmos.DrawLine(topLeft, topRight);
        Gizmos.DrawLine(topRight, bottomRight);
        Gizmos.DrawLine(bottomRight, bottomLeft);
    }
}

