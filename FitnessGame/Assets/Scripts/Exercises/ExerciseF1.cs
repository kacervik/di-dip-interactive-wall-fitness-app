using UnityEngine;

public class ExerciseF1 : ExerciseBase {
    [SerializeField] private float exerciseDuration = 30f;
    private readonly string exerciseName = "Reaction Time";
    private readonly string exerciseDescription = "Press as many as you can!";

    [SerializeField] private Vector3[] positions = new Vector3[8];
    [SerializeField] private float spacingAdjustement = 0.8f;
    [SerializeField] private float boundarySpacing;
    [SerializeField] private GameObject[] targetSockets;
    [SerializeField] private GameObject startButton;
    private Vector3 middle = (GameBoundary.instance.minBound + GameBoundary.instance.maxBound) / 2;
    private bool startPressed = false;
    private int lastPickedIndex = -1;


    private void Awake() {
        boundarySpacing = (GameBoundary.instance.maxBound.y - GameBoundary.instance.minBound.y) / 4;
        targetSockets = new GameObject[positions.Length];
        SetPositions();
        InstantiateTargetBoxesInPositions();
    }

    private void Update() {
        UpdateTargetPositions();

        if (startButton == null && startPressed == false) {
            isActive = true;
            for (int i = 0; i < targetSockets.Length; i++) {
                targetSockets[i].SetActive(true);
            }
            startPressed = true;
            UIManager.instance.ChangeBottomText(exerciseDescription);

        }

        if (isActive) {
            Countdown();
            // Logic of the exercise
            if (currentPrefabInstance == null) {
                SpawnPrefabs(PrefabLibrary.instance.prefabF1, PickTargetPosition());
            }
        } else { // Exercise Is Not Active
            if (startButton == null) { // If the game has started
                for (int i = 0; i < targetSockets.Length; i++) {
                    targetSockets[i].SetActive(false); // Disable whole array of target Sockets
                }

                // Update the UI after the game end.
                UIManager.instance.ClearUI();
                UIManager.instance.ChangeTopText("Your score was:");
                UIManager.instance.ChangeCenterText(ScoreManager.instance.GetF1Score().ToString());
                UIManager.instance.ChangeBottomText("Go to leaderboards to submit your score!");
                UIManager.instance.ShowMainMenuButton();
            }

        }
    }

    public override void StartExercise() {
        InitializeExercise(exerciseName, exerciseDuration);

        startButton = Instantiate(PrefabLibrary.instance.prefabF1SocketStart, new Vector3(middle.x, middle.y, 0), Quaternion.identity);

        UIManager.instance.ChangeScoreText("0");
        UIManager.instance.ChangeBottomText("Press the button to start the exercise");

        isActive = false;
    }

    private Vector3 PickTargetPosition() {
        int index;
        do {
            index = Random.Range(0, positions.Length);
        } while (index == lastPickedIndex && positions.Length > 1);

        lastPickedIndex = index;
        return positions[index];
    }


    private void SetPositions() {
        // Vector3 middle = (GameBoundary.instance.minBound + GameBoundary.instance.maxBound) / 2;

        positions[0] = new Vector3(middle.x + boundarySpacing * spacingAdjustement, middle.y + boundarySpacing * spacingAdjustement, 0);
        positions[1] = new Vector3(middle.x + 2 * boundarySpacing * spacingAdjustement, middle.y + 2 * boundarySpacing * spacingAdjustement, 0);
        positions[2] = new Vector3(middle.x - boundarySpacing * spacingAdjustement, middle.y + boundarySpacing * spacingAdjustement, 0);
        positions[3] = new Vector3(middle.x - 2 * boundarySpacing * spacingAdjustement, middle.y + 2 * boundarySpacing * spacingAdjustement, 0);
        positions[4] = new Vector3(middle.x - boundarySpacing * spacingAdjustement, middle.y - boundarySpacing * spacingAdjustement, 0);
        positions[5] = new Vector3(middle.x - 2 * boundarySpacing * spacingAdjustement, middle.y - 2 * boundarySpacing * spacingAdjustement, 0);
        positions[6] = new Vector3(middle.x + boundarySpacing * spacingAdjustement, middle.y - boundarySpacing * spacingAdjustement, 0);
        positions[7] = new Vector3(middle.x + 2 * boundarySpacing * spacingAdjustement, middle.y - 2 * boundarySpacing * spacingAdjustement, 0);
    }

    private void InstantiateTargetBoxesInPositions() {
        for (int i = 0; i < positions.Length; i++) {
            targetSockets[i] = Instantiate(PrefabLibrary.instance.prefabF1Socket, positions[i], Quaternion.identity);
        }
    }

    public void UpdateTargetPositions() {
        // Recalculate positions
        SetPositions();

        // Update positions of target boxes
        for (int i = 0; i < targetSockets.Length; i++) {
            targetSockets[i].transform.position = positions[i];
        }
    }
}
