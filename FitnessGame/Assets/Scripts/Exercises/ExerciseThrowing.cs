using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExerciseThrowing : ExerciseBase {
    [SerializeField] private float exerciseDuration = 30f;
    private string exerciseName = "Throwing";
    private string exerciseDescription = "Throw the ball to the target!";
    private Vector3 topVisiblePosition;

    private void Update() {
        if (isActive) {
            Countdown();
            if (currentPrefabInstance == null) {
                SpawnPrefabs(PrefabLibrary.instance.prefabThrowing, GenerateRandomPositionForThrowingTarget());
            }
        }
    }

    public override void StartExercise() {
        InitializeExercise(exerciseName, exerciseDuration, exerciseDescription);
    }

    // Generate a random position within the bounds of the exercise area, but limit it more to the top parts of the area
    private Vector3 GenerateRandomPositionForThrowingTarget() {
        topVisiblePosition = Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height, Camera.main.nearClipPlane));

        return new Vector3(Random.Range(GameBoundary.instance.minBound.x, GameBoundary.instance.maxBound.x),
                                      Random.Range(GameBoundary.instance.maxBound.y, topVisiblePosition.y),
                                                                0);
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.yellow;
        DrawRectangle(  new Vector3(GameBoundary.instance.minBound.x, GameBoundary.instance.maxBound.y, 0),
                        new Vector3(GameBoundary.instance.maxBound.x, topVisiblePosition.y, 0));
    }

    private void DrawRectangle(Vector3 bottomLeft, Vector3 topRight) {
        Vector3 topLeft = new Vector3(bottomLeft.x, topRight.y, 0);
        Vector3 bottomRight = new Vector3(topRight.x, bottomLeft.y, 0);

        Gizmos.DrawLine(bottomLeft, topLeft);
        Gizmos.DrawLine(topLeft, topRight);
        Gizmos.DrawLine(topRight, bottomRight);
        Gizmos.DrawLine(bottomRight, bottomLeft);
    }
}
