using UnityEngine;

public class ExerciseLegMovement : ExerciseBase {
    [SerializeField] private float exerciseDuration = 30f;
    private readonly string exerciseName = "Leg Movement";
    private readonly string exerciseDescription = "Return to base after each touch!";

    [SerializeField] private bool leftSide = true;
    [SerializeField] private float middle = (GameBoundary.instance.maxBound.x + GameBoundary.instance.minBound.x) / 2;
    [SerializeField] private float offset = 9f;

    private void Update() {
        if (isActive) {
            Countdown();
            if (currentPrefabInstance == null) {
                SpawnPrefabs(PrefabLibrary.instance.prefabLegMovement, GenerateRandomPositionForLegMovementTarget());
            }
        }
    }

    // Generates a random position within the boundaries of the game, but alternates left and right side of the wall
    private Vector3 GenerateRandomPositionForLegMovementTarget() {
        if (leftSide) { // Spawn on the left side from the middle of the boundaries
            leftSide = !leftSide;
            Vector3 left = new Vector3(Random.Range(GameBoundary.instance.minBound.x, middle - offset),
                                        Random.Range(GameBoundary.instance.minBound.y, GameBoundary.instance.maxBound.y),
                                        0);
            Debug.Log(left);
            return left;
        } else { // Spawn on the right side from the middle of the boundaries
            leftSide = !leftSide;
            Vector3 right = new Vector3(Random.Range(middle + offset, GameBoundary.instance.maxBound.x),
                                        Random.Range(GameBoundary.instance.minBound.y, GameBoundary.instance.maxBound.y),
                                        0);
            Debug.Log(right);
            return right;
        }
    }

    public override void StartExercise() {
        InitializeExercise(exerciseName, exerciseDuration, exerciseDescription);
    }

    private void OnDrawGizmos() {
        Gizmos.color = Color.magenta;
        DrawRectangle(new Vector3(GameBoundary.instance.minBound.x, GameBoundary.instance.minBound.y, 0),
                        new Vector3(middle - offset, GameBoundary.instance.maxBound.y, 0));

        Gizmos.color = Color.cyan;
        DrawRectangle(new Vector3(middle + offset, GameBoundary.instance.minBound.y, 0),
                        new Vector3(GameBoundary.instance.maxBound.x, GameBoundary.instance.maxBound.y, 0));
    }

    private void DrawRectangle(Vector3 bottomLeft, Vector3 topRight) {
        Vector3 topLeft = new Vector3(bottomLeft.x, topRight.y, 0);
        Vector3 bottomRight = new Vector3(topRight.x, bottomLeft.y, 0);

        Gizmos.DrawLine(bottomLeft, topLeft);
        Gizmos.DrawLine(topLeft, topRight);
        Gizmos.DrawLine(topRight, bottomRight);
        Gizmos.DrawLine(bottomRight, bottomLeft);
    }
}
