using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ExerciseManager : MonoBehaviour {
    [SerializeField] private Queue<ExerciseBase> exerciseQueue = new Queue<ExerciseBase>();

    private bool isFreestyleExperience = false;
    [SerializeField] private List<ExerciseBase> exerciseList = new List<ExerciseBase>();
    private int previousExerciseIndex = -1;

    private void Awake() {
        // Add every exercise to the list
        exerciseList.Add(gameObject.AddComponent<ExerciseThrowing>());
        exerciseList.Add(gameObject.AddComponent<ExerciseLegMovement>());
        exerciseList.Add(gameObject.AddComponent<ExerciseArmsMovement>());
        exerciseList.Add(gameObject.AddComponent<ExerciseLegRaises>());
        exerciseList.Add(gameObject.AddComponent<ExerciseCore>());
    }

    void Start() {
        UIManager.instance.ShowMainMenuButton();
        if (SceneManager.GetActiveScene().name == "ShortExperience") {
            StartShortExperience();
        } else if (SceneManager.GetActiveScene().name == "Freestyle") {
            StartFreestyleExperience();
        } else if (SceneManager.GetActiveScene().name == "F1") {
            StartF1Experience();
        }

        ScoreManager.instance.ResetScores();
    }

    private void StartShortExperience() {
        isFreestyleExperience = false;
        ScoreManager.instance.rememberScores = true;
        InitializeShortExperienceExerciseList();
        StartNextExercise();
    }

    private void StartFreestyleExperience() {
        isFreestyleExperience = true;
        ScoreManager.instance.rememberScores = false;
        StartNextExercise();
    }

    private void StartF1Experience() {
        isFreestyleExperience = false;
        ScoreManager.instance.rememberScores = true;
        InitializeF1ExperienceExerciseList();
        StartNextExercise();
    }

    private void InitializeShortExperienceExerciseList() {
        // Add exercises to the queue in the desired order
        exerciseQueue.Enqueue(gameObject.AddComponent<Tutorial>());
        exerciseQueue.Enqueue(gameObject.AddComponent<ExerciseLegMovement>());
        exerciseQueue.Enqueue(gameObject.AddComponent<ExerciseArmsMovement>());
        exerciseQueue.Enqueue(gameObject.AddComponent<ExerciseLegMovement>());
        exerciseQueue.Enqueue(gameObject.AddComponent<ExerciseThrowing>());
        exerciseQueue.Enqueue(gameObject.AddComponent<ExerciseLegRaises>());
        exerciseQueue.Enqueue(gameObject.AddComponent<ExerciseCore>());
    }

    private void InitializeF1ExperienceExerciseList() {
        exerciseQueue.Enqueue(gameObject.AddComponent<ExerciseF1>());
    }

    public void StartNextExercise() {
        if (isFreestyleExperience) { // Random exercise
            ScoreManager.instance.ResetScores();
            int random = Random.Range(0, exerciseList.Count);
            if (random == previousExerciseIndex) {
                random = (random + 1) % exerciseList.Count;
            }
            ExerciseBase nextExercise = exerciseList[random];
            nextExercise.StartExercise();
        } else { // Short experience in predetermined order
            if (exerciseQueue.Count > 0) {
                ExerciseBase nextExercise = exerciseQueue.Dequeue();
                nextExercise.StartExercise();
            } else {
                Debug.Log("All exercises completed!");

                UIManager.instance.ChangeTopText("All exercises completed!");
                UIManager.instance.ChangeCenterText(ScoreManager.instance.GetShortExperienceScore().ToString());
                UIManager.instance.ChangeBottomText("Return to main menu.");


                // return to main menu in 5 seconds in asynchronous way
                StartCoroutine(ReturnToMainMenu());
            }
        }
    }

    private IEnumerator ReturnToMainMenu() {
        yield return new WaitForSeconds(5);
        SceneManager.LoadScene("MainMenu");
    }
}
