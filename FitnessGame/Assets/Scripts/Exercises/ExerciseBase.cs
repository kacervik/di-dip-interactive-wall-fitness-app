using UnityEngine;

public abstract class ExerciseBase : MonoBehaviour {
    public float countdown = 0;
    public bool isCountdownActive = false;
    public bool isActive = false;

    public GameObject currentPrefabInstance = null;

    public abstract void StartExercise();
    public virtual void EndExercise() {
        if (currentPrefabInstance != null) {
            Destroy(currentPrefabInstance);
        }

        isActive = false;
        UIManager.instance.ChangeCenterText("");
        FindObjectOfType<ExerciseManager>().StartNextExercise();
    }

    public void StartCountdown(float exerciseDuration) {
        isCountdownActive = true;
        countdown = exerciseDuration + 1; // adding one second to the exercise so that the counter starts (exercuseDuration) and ends (0) at a round number 
    }

    public void Countdown() {
        countdown -= Time.deltaTime;
        if (countdown < 0) {
            countdown = 0;
            EndExercise();
        }
        UIManager.instance.ChangeTimerText(Mathf.Floor(countdown).ToString());
    }

    public void SpawnPrefabs(GameObject prefabToSpawn, Vector3 position) {
        currentPrefabInstance = Instantiate(prefabToSpawn, position, Quaternion.identity);
    }

    public Vector3 GenerateRandomPositionInBounds() {
        return new Vector3(Random.Range(GameBoundary.instance.minBound.x, GameBoundary.instance.maxBound.x),
                                      Random.Range(GameBoundary.instance.minBound.y, GameBoundary.instance.maxBound.y),
                                                                0);
    }

    public void InitializeExercise(string exerciseName, float exerciseDuration, string exerciseDescription = "") {
        UIManager.instance.ChangeTopText(exerciseName);
        UIManager.instance.ChangeBottomText(exerciseDescription);
        StartCountdown(exerciseDuration);
        isActive = true;
    }
}
