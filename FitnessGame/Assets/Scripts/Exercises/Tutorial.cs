using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : ExerciseBase {
    [SerializeField] private float exerciseDuration = 3f;
    private string exerciseName = "Get ready in...";


    private void Start() {
        // start coroutine to do a countdown
        StartCoroutine(CountdownCoroutine());
    }

    private void Update() {
        if (isActive) {
            // Logic of the exercise
            Countdown();
            // TODO
            UIManager.instance.ChangeTimerText("");
            // UIManager.instance.ChangeCenterText(Mathf.Floor(countdown).ToString());
        }
    }

    public override void StartExercise() {
        InitializeExercise(exerciseName, exerciseDuration);
    }    

    private IEnumerator CountdownCoroutine() {
        UIManager.instance.ChangeCenterText("3");
        yield return new WaitForSeconds(1);
        UIManager.instance.ChangeCenterText("2");
        yield return new WaitForSeconds(1);
        UIManager.instance.ChangeCenterText("1");
        yield return new WaitForSeconds(1);
        UIManager.instance.ChangeCenterText("Go!");
        yield return new WaitForSeconds(1);
        UIManager.instance.ChangeCenterText("");
    }
}
