using System.Collections;
using UnityEngine;

public class hitIndicatorAutodestroy : MonoBehaviour {
    public float fadeDuration = 0.25f;

    private void Start() {
        StartCoroutine(DestroyAfterTime());
    }

    private IEnumerator DestroyAfterTime() {
        float startTime = Time.time;

        SpriteRenderer renderer = GetComponent<SpriteRenderer>();

        while (Time.time - startTime < fadeDuration) {
            // Calculate how much time has passed since we started fading
            float t = (Time.time - startTime) / fadeDuration;

            if (renderer != null) {
                Color color = renderer.color;
                color.a = Mathf.Lerp(1f, 0f, t);
                renderer.color = color;
            }

            yield return null;
        }

        Destroy(gameObject);
    }
}
