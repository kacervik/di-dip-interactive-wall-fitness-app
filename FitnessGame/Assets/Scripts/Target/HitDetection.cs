using UnityEngine;

public class HitDetection : BaseInteractive {
    [SerializeField] private LayerMask layerMask; // LayerMask to filter collisions against
    private AudioSource audioSource;

    private void Start() {
        audioSource = GetComponent<AudioSource>();
        audioSource.playOnAwake = false;
    }


    public override void OnPress(Vector2 hitPosition) {
        // Increase the appropriate score based on the tag of the hit object
        if (ScoreManager.instance != null) {
            if (this.tag == "F1Experience") {
                ScoreManager.instance.IncreaseF1Score();
            } else if (this.tag == "ShortExperience") {
                ScoreManager.instance.IncreaseExperienceScore();
            }
        }

        audioSource.pitch = Random.Range(0.9f, 1.1f);
        audioSource.Play(); // Play the audio clip

        // Destroy object
        Destroy(this.gameObject);
    }
}
