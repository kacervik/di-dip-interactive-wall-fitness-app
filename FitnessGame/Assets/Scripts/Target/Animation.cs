using System.Collections;
using UnityEngine;

public class Animation : MonoBehaviour {
    public bool changePosition = false;

    private float duration = 0.1f; // Duration of the effect
    private SpriteRenderer spriteRenderer;
    private Vector3 initialScale;
    private Vector3 initialLocation;


    private void Start() {
        // Start the effect when the object is instantiated
        spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        initialScale = transform.localScale; // Store the initial scale
        transform.localScale = Vector3.zero; // Set the initial scale to 0

        if (changePosition) {
            initialLocation = transform.position; // Store the initial location
            float x = initialLocation.x + Mathf.Abs(initialLocation.x) * UnityEngine.Random.Range(0f, 0.6f);
            float y = initialLocation.y + UnityEngine.Random.Range(0f, 0.1f);
            transform.position = new Vector3(x, y, 0);
        }

        StartCoroutine(StartEffect());
    }

    private IEnumerator StartEffect() {
        float startTime = Time.time;

        while (Time.time - startTime <= duration) {
            float t = (Time.time - startTime) / duration;

            // Gradually increase the scale from 0 to initial scale
            transform.localScale = initialScale * t;
            if (changePosition) {
                transform.localPosition = initialLocation * t;
            }

            // Get the SpriteRenderer component from the children of the current GameObject
            if (spriteRenderer != null) {
                Color color = spriteRenderer.color;
                color.a = t;
                spriteRenderer.color = color;
            }

            yield return null;
        }

        if (Time.time - startTime > duration) {
            transform.localScale = initialScale; // Set the scale to the initial scale
            if (changePosition) {
                transform.localPosition = initialLocation;
            }
        }
    }
}
