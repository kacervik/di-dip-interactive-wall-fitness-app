using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetScalingWithResolution : MonoBehaviour {
    [SerializeField] private float scaleFactor = 1.0f;

    private void Awake() {
        if (Screen.width == 1920) {
            scaleFactor = 1.0f;
        } else if (Screen.width == 3840) {
            scaleFactor = 2.0f;
        }

        transform.localScale *= scaleFactor;
    }
}
