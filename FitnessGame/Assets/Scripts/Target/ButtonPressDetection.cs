using UnityEngine;
using UnityEngine.Events;

public class ButtonPressDetection : BaseInteractive {
    [Header("Button Press Events")]
    public UnityEvent onClick;

    public override void OnHold(Vector2 hitPosition) {
        onClick.Invoke();
    }
}
