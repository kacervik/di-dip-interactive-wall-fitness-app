using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabLibrary : MonoBehaviour {
    public static PrefabLibrary instance;

    public GameObject prefabBase;
    public GameObject prefabLegMovement;
    public GameObject prefabArmMovement;
    public GameObject prefabThrowing;
    public GameObject prefabF1;
    public GameObject prefabF1Socket;
    public GameObject prefabF1SocketStart;
    public GameObject hitIndicator;
    public GameObject silhouette;


    void Awake() {
        if (instance == null) {
            instance = this;
            DontDestroyOnLoad(gameObject);
        } else {
            Destroy(gameObject);
        }
    }
}
