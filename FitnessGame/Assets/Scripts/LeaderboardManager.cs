using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Dan.Main;

public class LeaderboardManager : MonoBehaviour {
    public static LeaderboardManager instance;

    [SerializeField] private List<TextMeshProUGUI> names;
    [SerializeField] private List<TextMeshProUGUI> scores;

    private string publicF1LeadebroardKey = "c774a5313107484d38ceb98d9e626530af6bbea8708059d7536d0a5be8823976";
    private string publicShortExperienceLeaderboardKey = "dd952a579bc90916413c89510fcd8e4d54652bda32ce5ee49f74f22a4f1a658a";
    
    [SerializeField] private TMP_InputField usernameInput;
    [SerializeField] private TextMeshProUGUI score;

    private void Awake() {
        if (instance == null) {
            instance = this;
            // DontDestroyOnLoad(gameObject);
        } else {
            Destroy(gameObject);
        }


        // Disable all entries in the leaderboard
        for (int i = 0; i < names.Count; i++) {
            names[i].gameObject.SetActive(false);
            scores[i].gameObject.SetActive(false);
        }

        score.text = ScoreManager.instance.GetShortExperienceScore().ToString();
        GetShortExperienceLeaderboard();
    }

    public void GetF1Leaderboard() {
        score.text = ScoreManager.instance.GetF1Score().ToString(); 
        LeaderboardCreator.GetLeaderboard(publicF1LeadebroardKey, (leaderboard) => {
            int entriesNum = (leaderboard.Length > names.Count) ? names.Count : leaderboard.Length;
            for (int i = 0; i < entriesNum; i++) {
                names[i].gameObject.SetActive(true);
                names[i].text = leaderboard[i].Username;

                scores[i].gameObject.SetActive(true);
                scores[i].text = leaderboard[i].Score.ToString();
            }

            // Disable every entry that is not in the leaderboard
            for (int i = leaderboard.Length; i < names.Count; i++) {
                names[i].gameObject.SetActive(false);
                scores[i].gameObject.SetActive(false);
            }
        });
    }

    public void GetShortExperienceLeaderboard() {
        score.text = ScoreManager.instance.GetShortExperienceScore().ToString();
        LeaderboardCreator.GetLeaderboard(publicShortExperienceLeaderboardKey, (leaderboard) => {
            int entriesNum = (leaderboard.Length > names.Count) ? names.Count : leaderboard.Length;
            for (int i = 0; i < entriesNum; i++) {
                names[i].gameObject.SetActive(true);
                names[i].text = leaderboard[i].Username;

                scores[i].gameObject.SetActive(true);
                scores[i].text = leaderboard[i].Score.ToString();
            }

            // Disable every entry that is not in the leaderboard
            for (int i = leaderboard.Length; i < names.Count; i++) {
                names[i].gameObject.SetActive(false);
                scores[i].gameObject.SetActive(false);
            }
        });
    }

    public void UploadNewF1LeaderboardEntry() {
        int scoreToUpload = ScoreManager.instance.GetF1Score();     // Get score to upload
        string nameToUpload = usernameInput.text;                   // Get username to upload

        Leaderboards.F1ReactionSpeedLeaderboard.ResetPlayer();      // Non unique names and players, enables to upload multiple entries from one device 
        LeaderboardCreator.UploadNewEntry(publicF1LeadebroardKey, nameToUpload, scoreToUpload, ((msg) => {
            GetF1Leaderboard();
        }));
    }

    public void UpdateLastF1LeaderboardEntry() {
        int scoreToUpload = ScoreManager.instance.GetF1Score();     // Get score to upload
        string nameToUpload = usernameInput.text;                   // Get username to upload

        // Leaderboards.F1ReactionSpeedLeaderboard.ResetPlayer();      // Non unique names and players, enables to upload multiple entries from one device 
        LeaderboardCreator.UploadNewEntry(publicF1LeadebroardKey, nameToUpload, scoreToUpload, ((msg) => {
            GetF1Leaderboard();
        }));
    }

    public void UploadNewShortExperienceLeaderboardEntry() {
        int scoreToUpload = ScoreManager.instance.GetShortExperienceScore();     // Get score to upload
        string nameToUpload = usernameInput.text;                   // Get username to upload

        Leaderboards.ShortExperienceLeaderboard.ResetPlayer();      // Non unique names and players, enables to upload multiple entries from one device 
        LeaderboardCreator.UploadNewEntry(publicShortExperienceLeaderboardKey, nameToUpload, scoreToUpload, ((msg) => {
            GetShortExperienceLeaderboard();
        }));
    }

    public void UpdateLastShortExperienceLeaderboardEntry() {
        int scoreToUpload = ScoreManager.instance.GetShortExperienceScore();     // Get score to upload
        string nameToUpload = usernameInput.text;                   // Get username to upload

        LeaderboardCreator.UploadNewEntry(publicShortExperienceLeaderboardKey, nameToUpload, scoreToUpload, ((msg) => {
            GetShortExperienceLeaderboard();
        }));
    }
}
