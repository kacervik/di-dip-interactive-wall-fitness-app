using UnityEngine;

public class ScoreManager : MonoBehaviour {
    [Header("Scores")]
    [SerializeField] private int F1Score = 0;
    [SerializeField] private int ShortExperienceScore = 0;

    public static ScoreManager instance;
    public bool rememberScores = false;


    void Awake() {
        if (instance == null) {
            instance = this;
            DontDestroyOnLoad(gameObject);
        } else {
            Destroy(gameObject);
        }
    }

    public void ResetScores() {
        F1Score = 0;
        ShortExperienceScore = 0;
    }

    public void IncreaseF1Score(int score = 1) {
        if (rememberScores) {
            F1Score += score;
            UIManager.instance.ChangeScoreText(F1Score.ToString());
        }
    }

    public int GetF1Score() {
        return F1Score;
    }

    public void IncreaseExperienceScore(int score = 1) {
        if (rememberScores) {
            ShortExperienceScore += score;
            UIManager.instance.ChangeScoreText(ShortExperienceScore.ToString());
        }
    }

    public int GetShortExperienceScore() {
        return ShortExperienceScore;
    }
}
