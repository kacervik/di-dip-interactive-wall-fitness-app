using UnityEngine;

public class AudioManager : MonoBehaviour {
    public AudioClip[] songs;
    private AudioSource audioSource;
    private int currentIndex = 0;

    void Start() {
        audioSource = GetComponent<AudioSource>();
        ShuffleSongs();
        PlayNextSong();
    }

    void ShuffleSongs() {
        // Fisher-Yates shuffle algorithm
        for (int i = 0; i < songs.Length; i++) {
            int randomIndex = Random.Range(i, songs.Length);
            AudioClip tempClip = songs[randomIndex];
            songs[randomIndex] = songs[i];
            songs[i] = tempClip;
        }
    }

    void PlayNextSong() {
        audioSource.clip = songs[currentIndex];
        audioSource.loop = false;
        audioSource.Play();

        currentIndex = (currentIndex + 1) % songs.Length; // Loop back to the beginning if reached the end
    }

    void Update() {
        if (!audioSource.isPlaying) {
            PlayNextSong();
        }
    }
}
