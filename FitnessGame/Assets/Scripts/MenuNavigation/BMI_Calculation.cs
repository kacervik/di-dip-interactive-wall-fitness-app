using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BMI_Calculation : MonoBehaviour {
    public void CalculateBMI() {
        float weight = float.Parse(GameObject.Find("WeightInput").GetComponent<UnityEngine.UI.InputField>().text);
        float height = float.Parse(GameObject.Find("HeightInput").GetComponent<UnityEngine.UI.InputField>().text);
        float bmi = weight / (height * height);
        GameObject.Find("BMIResult").GetComponent<UnityEngine.UI.Text>().text = bmi.ToString();
    }
}
