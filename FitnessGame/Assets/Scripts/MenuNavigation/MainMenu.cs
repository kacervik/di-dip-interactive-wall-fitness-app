using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MainMenu : MonoBehaviour {
    public static MainMenu instance;

    [Header("Main Menu Elements")]
    public GameObject mainMenu;
    public GameObject exercises;
    public GameObject introduction;
    public GameObject leaderboards;

    [Header("Leaderboards")]
    public GameObject ShortLeaderboard;
    public GameObject F1Leaderboard;

    private void Awake() {
        if (instance == null) {
            instance = this;
        } else {
            Destroy(gameObject);
        }

        UIManager.instance.ClearUI();
    }

    private void Start() {
        ShowMainMenu();
    }

    public void ShowMainMenu() {
        mainMenu.SetActive(true);
        exercises.SetActive(false);
        introduction.SetActive(false);
        leaderboards.SetActive(false);
    }

    public void ShowExercises() {
        mainMenu.SetActive(false);
        exercises.SetActive(true);
    }

    public void ShowIntroduction() {
        mainMenu.SetActive(false);
        introduction.SetActive(true);
    }

    public void ShowLeaderboards() {
        mainMenu.SetActive(false);
        leaderboards.SetActive(true);
        ShortLeaderboard.SetActive(true);
        F1Leaderboard.SetActive(false);
    }

    public void LoadShortExperience() {
        UnityEngine.SceneManagement.SceneManager.LoadScene("ShortExperience");
    }

    public void LoadFreestyleExperience() {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Freestyle");
    }

    public void LoadF1Experience() {
        UnityEngine.SceneManagement.SceneManager.LoadScene("F1");
    }

    public void LoadBoundariesSetting() {
        GameBoundary.instance.Reset();
        UnityEngine.SceneManagement.SceneManager.LoadScene("BoundarySetting");
    }

    public void ToggleLeaderboards() {
        if (ShortLeaderboard.activeSelf) {
            ShortLeaderboard.SetActive(false);
            F1Leaderboard.SetActive(true);
            LeaderboardManager.instance.GetF1Leaderboard();
        } else {
            ShortLeaderboard.SetActive(true);
            F1Leaderboard.SetActive(false);
            LeaderboardManager.instance.GetShortExperienceLeaderboard();
        }
    }



    public void QuitGame() {
        Application.Quit();
    }
}
