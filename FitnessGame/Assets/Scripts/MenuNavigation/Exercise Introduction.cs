using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExerciseIntroduction : MonoBehaviour {
    [SerializeField] private GameObject[] images;
    private int currentImageIndex = 0;

    private void Start() {
        ShowCurrentImage();
    }

    public void NextImage() {
        currentImageIndex++;
        if (currentImageIndex >= images.Length) {
            currentImageIndex = 0;
        }
        ShowCurrentImage();
    }

    public void PreviousImage() {
        currentImageIndex--;
        if (currentImageIndex < 0) {
            currentImageIndex = images.Length - 1;
        }
        ShowCurrentImage();
    }

    private void ShowCurrentImage() { 
        for (int i = 0; i < images.Length; i++) {
            images[i].SetActive(false);
        }
        images[currentImageIndex].SetActive(true);
    }
}
