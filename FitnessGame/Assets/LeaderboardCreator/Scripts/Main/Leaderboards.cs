namespace Dan.Main
{
    public static class Leaderboards
    {
        public static LeaderboardReference F1ReactionSpeedLeaderboard = new LeaderboardReference("c774a5313107484d38ceb98d9e626530af6bbea8708059d7536d0a5be8823976");
        public static LeaderboardReference ShortExperienceLeaderboard = new LeaderboardReference("dd952a579bc90916413c89510fcd8e4d54652bda32ce5ee49f74f22a4f1a658a");
    }
}
