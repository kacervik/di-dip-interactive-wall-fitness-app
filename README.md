# Fitness Game
This is an implementation of my diploma thesis, a fitness game made in unity game engine.

To integrate INITI Wall functionality, follow these steps:

1. Find an example Unity project for the INITI wall or any other Unity project that utilizes INITI functionality.
2. Then find the INITI folder in the asset browser, right-click on it, and select *Export Package...*.
3. Create or open your own project and select *Import Package/Custom Package...* or drag and drop the exported file.
4. In the script file, don't forget to include `using initi.prefabScripts;` at the top. Change the inheritance from MonoBehaviour to BaseHittable, which allows you to use the `Hit()` method to detect touch on the wall.
5. Add the script that contains touch/hit logic to an asset/prefab and make sure the prefab has a collider attached; without it, touch detection won't work.
6. Finally, add the *Input* prefab from *Assets/Initi/Prefabs* to every scene where you want to detect touch.

With those steps complete, the touch should be correctly mapped to Unity input, but there are several tips to make integration, development, and testing more enjoyable.

- Download the TUIO simulator from [this GitHub repository](https://github.com/gregharding/TUIOSimulator/releases)~\cite{tuio_simulator}. This allows you to test touch inputs from the wall at home by simulating touch inputs. After downloading, turn on the simulator and configure the *Server port to 33333* and *Local Port to 12345*; the rest can be left unchanged. Next, open Unity and enable *Edit/Project Settings/Player/Resolution and Presentation/Run in Background*. This ensures that the game won't freeze while using the simulator. Then, find the *Input* prefab, press the plus button in the input lists, and add *TuioInput*. With all these steps completed, turn on the simulator and your game, and the inputs should get mapped accordingly.


Build your game.
1. Build your game.
2. Create two thumbnails for the game:
    - A small thumbnail that shows in the game selection screen with a resolution of 640x390. Save it as buttonImage.png.
    - A larger preview of the game that is shown when the game is selected with a resolution of 3235x1770. Save it as windowImage.png.
3. Locate the INITI folder in the media server, either in C:\initi or on the desktop as a shortcut.
4. Place the thumbnails into the folder where your game is built and place the entire build 5. into the INITI folder.
6. Open the file ui\games.json and add the name of your game/folder to the existing list.
7. Restart INITI playground and start playing!

### Music License
Music from #Uppbeat (free for Creators!):

https://uppbeat.io/t/matrika/overdrive

License code: 1V7AJSDXBEHQHUWU
